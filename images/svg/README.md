# SVG image uploads
SVG images placed in here are not actually loaded. Instead their inner XML is
placed directly onto HTML pages or into the svg-template.js script.
