/* eslint-disable no-unused-vars */

api = 'https://api.mreasygoing.com';
gitlabApi = 'https://gitlab.com/api';
projectId = 5227062;
projectName = 'nicholaschiasson.gitlab.io';
projectOwner = 'nicholasomerchiasson@gmail.com';
rootResourceId = '0B7alA-hFE6KLV0U5bm1nQzh1WW8';

impersonationResource = `/v1/impersonation/gitlab/actions`;
projectsResource = '/v4/projects';

eventInitializedName = 'initialized';
eventRefreshTokenName = 'refreshtoken';

md = window.markdownit().set({html: true});

const googleAccountsApi = 'https://accounts.google.com';

const authResource = '/o/oauth2/v2/auth';
const gitlabClientIdsResource = '/v1/oauth/gitlab/clientIds';
const googleClientIdsResource = '/v1/oauth/google/clientIds';
const googleAccessTokensResource = '/v1/oauth/google/accessTokens';

const eventInitialized = new Event(eventInitializedName);
const eventRefreshToken = new Event(eventRefreshTokenName);

appendElementWithStringAsset = function(element, assetName, assetString) {
	let renderText = assetName.split('.').pop() === 'md' ? md.render(assetString) : assetString;
	let template = createDOMElement('template', null, renderText);
	for (let i = 0; i < template.content.childNodes.length; i++) {
		let node = template.content.childNodes[i];
		switch (node.nodeName.toLowerCase()) {
			case 'title':
			case 'style':
			case 'meta':
			case 'link':
			case 'script':
			case 'base':
				document.head.appendChild(node);
				break;
			default:
				element.appendChild(node);
				break;
		}
	}
};

isMobile = function() {
	let browser = navigator.userAgent || navigator.vendor || window.opera;
	// Original
	// return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(browser) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(browser.substr(0,4));
	// Removed "Unnecessary escape characters"
	return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(browser) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw-(n|u)|c55\/|capi|ccwa|cdm-|cell|chtm|cldc|cmd-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc-s|devi|dica|dmob|do(c|p)o|ds(12|-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(-|_)|g1 u|g560|gene|gf-5|g-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd-(m|p|t)|hei-|hi(pt|ta)|hp( i|ip)|hs-c|ht(c(-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i-(20|go|ma)|i230|iac( |-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|-[a-w])|libw|lynx|m1-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|-([1-8]|c))|phil|pire|pl(ay|uc)|pn-2|po(ck|rt|se)|prox|psio|pt-g|qa-a|qc(07|12|21|32|60|-[2-7]|i-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h-|oo|p-)|sdk\/|se(c(-|0|1)|47|mc|nd|ri)|sgh-|shar|sie(-|m)|sk-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h-|v-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl-|tdg-|tel(i|m)|tim-|t-mo|to(pl|sh)|ts(70|m-|m3|m5)|tx-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas-|your|zeto|zte-/i.test(browser.substr(0,4));
};

cacheGet = function(key, onNotInCache, clear) {
	if (clear) {
		delete localStorage[key];
	}
	if (localStorage[key]) {
		console.debug(`cache[${key}]: hit`);
		return Promise.resolve(localStorage[key]);
	}
	return new Promise((resolve, reject) => {
		console.debug(`cache[${key}]: miss`);
		onNotInCache()
			.then(res => {
				localStorage[key] = res;
				resolve(res);
			})
			.catch(reject);
	});
};

tryOrInvalidate = function(cb, catchIntercept) {
	return cb().catch(err => {
		if (catchIntercept) {
			return catchIntercept(err, () => cb(true));
		}
		return cb(true);
	});
};

request = function(method, url, params, headers, body, mimeType, noCache) {
	if (noCache) {
		if (!params) params = {};
		params._ = new Date().getTime();
	}
	let uriLocation = encodeURIWithQuery(url, params);
	// Return a new promise.
	return new Promise(function(resolve, reject) {
		// Do the usual XHR stuff
		let req = new XMLHttpRequest();
		req.open(method, uriLocation);

		req.overrideMimeType(mimeType || 'text/plain');

		for (let h in headers) {
			req.setRequestHeader(h, typeof headers[h] !== 'string' ? JSON.stringify(headers[h]) : headers[h]);
		}

		req.onload = function() {
			// This is called even on 404 etc
			// so check the status
			if (req.status >= 200 && req.status < 400) {
				// Resolve the promise with the response text
				resolve(req.response);
			}
			else {
				// Otherwise reject with the status text
				// which will hopefully be a meaningful error
				reject(Error(req.statusText));
			}
		};

		// Handle network errors
		req.onerror = function() {
			reject(Error(`${method.toUpperCase()} ${uriLocation}`));
		};

		// Make the request
		req.send(typeof body !== 'string' ? JSON.stringify(body) : body);
	});
};

createDOMElement = function(type, attributes, html) {
	let newElement = document.createElement(type);
	for (let a in attributes) {
		newElement.setAttribute(a, attributes[a]);
	}
	if (html) {
		newElement.innerHTML = html;
	}
	return newElement;
};

dropURLHash = function() {
	window.location.hash = '';
};

getURLParameters = function() {
	return Object.assign({}, getURLQueryParameters(), getURLHashParameters());
};

getURLQueryParameters = function() {
	return getEncodedParameters(window.location.search);
};

getURLHashParameters = function() {
	return getEncodedParameters(window.location.hash.substring(1));
};

getEncodedParameters = function(encodedString) {
	let params = {};
	for (let p of new URLSearchParams(encodedString)) {
		params[p[0]] = p[1];
	}
	return params;
};

getURLParameter = function(name) {
	return getURLQueryParameter(name) || getURLHashParameter(name);
};

getURLQueryParameter = function(name) {
	return getEncodedParameter(window.location.search, name);
};

getURLHashParameter = function(name) {
	return getEncodedParameter(window.location.hash, name);
};

function getEncodedParameter(encodedString, parameterName) {
	return decodeURIComponent((new RegExp('[?|#|&]' + parameterName + '=' + '([^&;]+?)(&|#|;|$)').exec(encodedString) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

function encodeURIWithQuery(uri, query) {
	return `${encodeURI(uri)}${encodeQueryData(query)}`;
}

function encodeQueryData(data) {
	let ret = [];
	for (let d in data)
		ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
	return ret.length > 0 ? `?${ret.join('&')}` : '';
}

gitlabAuthenticate = function() {
	request('GET', `${api}${gitlabClientIdsResource}`, {application: projectName})
		.then(function(res) {
			window.location = encodeURIWithQuery('https://gitlab.com/oauth/authorize', {
				client_id: res,
				redirect_uri: `https://${projectName}/auth.html`,
				response_type: 'code',
				state: `provider=gitlab&origin_url=${window.location}`
			});
		})
		.catch(function(err) {
			alert(err + ': Failed to initiate authentication.');
		});
};

googleAuthenticate = function() {
	request('GET', `${api}${googleClientIdsResource}`, {application: projectName})
		.then(function(res) {
			window.location = encodeURIWithQuery(`${googleAccountsApi}${authResource}`, {
				client_id: res,
				redirect_uri: `https://${projectName}/auth.html`,
				response_type: 'code',
				access_type: 'offline',
				scope: 'https://www.googleapis.com/auth/drive.readonly',
				state: `provider=google&origin_url=${window.location}`,
				prompt: 'consent'
			});
		})
		.catch(function(err) {
			alert(err + ': Failed to initiate authentication.');
		});
};

googleRefreshToken = function() {
	return new Promise(function(resolve, reject) {
		if (localStorage.googleAccessToken) {
			let token = JSON.parse(localStorage.googleAccessToken);
			if (token.refresh_token) {
				request('GET', `${api}${googleAccessTokensResource}`, {application: projectName, refresh_token: token.refresh_token})
					.then(function(res) {
						let accessToken = JSON.parse(res);
						if (!accessToken.access_token) throw new Error('Error: Response body does not contain access_token');
						localStorage.googleAccessToken = JSON.stringify(Object.assign(token, accessToken));
						window.dispatchEvent(eventRefreshToken);
						setTimeout(googleRefreshToken, accessToken.expires_in / 2 * 1000);
						resolve(true);
					})
					.catch(function(err) {
						setTimeout(googleRefreshToken, 8000);
						reject(err + ': Failed to refresh access token.');
					});
			} else {
				resolve(false);
				googleAuthenticate();
			}
		} else {
			resolve(false);
			googleAuthenticate();
		}
	});
};

function initialize(pageContent) {
	enforceHTTPS(); // Should always be the first thing we do
	request('GET', `${api}/v1`); // Simple request to wake up API but don't care about response
	window.addEventListener('resize', onWindowResize, true);
	window.addEventListener('orientationchange', onWindowResize, true);
	window.addEventListener('load', function() { onWindowLoad(pageContent); }, true);
}

/* eslint-enable no-unused-vars */

function enforceHTTPS() {
	if (window.location.hostname !== 'localhost' && window.location.protocol !== 'https:') {
		window.location.protocol = 'https:';
	}
}

function onWindowResize() {
	if (!isMobile()) {
		let wrapperDiv = document.getElementById('wrapper');
		const wrapperDivWidth = Math.max(Math.min(window.innerWidth, window.innerHeight), 0.7 * window.innerWidth) / window.innerWidth * 100.0;
		wrapperDiv.style.width = wrapperDivWidth + '%';
		wrapperDiv.style.marginLeft = (100.0 - wrapperDivWidth) / 2.0 + '%';
	}
}

function onWindowLoad(page) {
	onWindowResize();

	if (!page)
		page = 'home.html';

	let filenames = [];
	filenames.push('views/root/header.html');
	filenames.push('views/root/navigation.html');
	filenames.push(page);
	filenames.push('views/root/footer.html');

	let promises = [];
	for (let i = 0; i < filenames.length; i++) {
		promises.push(request('GET', filenames[i]));
	}

	Promise.all(promises)
		.then(function(response) {
			let wrapperDiv = document.getElementById('wrapper');
			for (let i = 0; i < response.length; i++) {
				let newDiv = wrapperDiv;
				if (filenames[i].endsWith('.md')) {
					newDiv = createDOMElement('div', {'id': 'content'});
					wrapperDiv.appendChild(newDiv);
				}
				appendElementWithStringAsset(newDiv, filenames[i], response[i]);
			}

			// Request repository metadata from GitLab API if not cached for the session
			// Use metadata to applying last push date as copyright year for all pages
			request('GET', `${gitlabApi}${projectsResource}/${projectId}`)
				.then(function(response) {
					processRepoMeta(response);
				});

			// Setting active page to determine which tab to highlight on page
			let pageName = window.location.pathname.split('/').pop().split('.')[0] || 'index';
			let activeNavigationButton = document.getElementById('nav-button-' + pageName);
			if (activeNavigationButton) {
				activeNavigationButton.setAttribute('class', 'active');
			}

			// Window initialization complete
			window.dispatchEvent(eventInitialized);
		})
		.catch(function(error) {
			console.log(error);
			window.location.href = '404.html';
		});
}

function processRepoMeta(repoMeta) {
	let meta = JSON.parse(repoMeta);
	let copyrightYear = document.getElementById('year-of-last-update');
	if (copyrightYear && meta && meta.last_activity_at) {
		copyrightYear.innerHTML = new Date(meta.last_activity_at).getFullYear();
	}
}
