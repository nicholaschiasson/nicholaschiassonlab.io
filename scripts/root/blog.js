// jshint esversion: 6

const issuesResource = `${projectsResource}/${projectId}/issues`;
const userUrl = '/v4/user';

/* eslint-disable no-unused-vars */
let postComment;
let deleteComment;
/*eslint-enable no-unused-vars */

function renderBlogList() {
	let contentDiv = document.getElementById('content');

	function createBlogEntryListItem(entry) {
		let item = createDOMElement('li');
		item.appendChild(createDOMElement('a', {'class': 'entry-title', 'href': `${window.location}?id=${entry.id}`}, entry.title));
		item.appendChild(createDOMElement('br'));
		let date_p = createDOMElement('p', {'class': 'creation-date'});
		let created_span = createDOMElement('span', {
			'class': 'date-time', 'data-toggle': 'tooltip', 'title': new Date(entry.created_at).toTimeString()
		}, new Date(entry.created_at).toDateString());
		date_p.appendChild(created_span);
		if (entry.created_at !== entry.updated_at) {
			let updated_span = createDOMElement('span', {
				'class': 'date-time', 'data-toggle': 'tooltip', 'title': new Date(entry.updated_at).toTimeString()
			}, new Date(entry.updated_at).toDateString());
			date_p.innerHTML += ' (Updated: ';
			date_p.appendChild(updated_span);
			date_p.innerHTML += ')';
		}
		item.appendChild(date_p);
		return item;
	}

	function createBlogList(entries) {
		let list = createDOMElement('ul');
		for (let i = 0; i < entries.length; i++) {
			list.appendChild(createBlogEntryListItem(entries[i]));
		}
		return list;
	}

	function renderBlogEntry(entryMeta) {
		contentDiv.innerHTML = '';
		let returnButton = createDOMElement('a', {'class': 'back-button', 'href': 'blog.html'});
		let returnButtonInner = createDOMElement('h5', null, 'back to list');
		returnButton.appendChild(returnButtonInner);
		contentDiv.appendChild(returnButton);
		let blogTitle = createDOMElement('h1', {'class': 'blog-title'}, entryMeta.title);
		contentDiv.appendChild(blogTitle);
		let originalPost = createDOMElement('div');
		let opAvatarAnchor = createDOMElement('a', {'class': 'user-avatar-anchor', 'href': entryMeta.author.web_url, 'target': '_blank'});
		let opAvatar = createDOMElement('img', {'class': 'user-avatar original-post-avatar', 'src': entryMeta.author.avatar_url});
		opAvatarAnchor.appendChild(opAvatar);
		originalPost.appendChild(opAvatarAnchor);
		let opInfo = createDOMElement('span', {'class': 'original-post-info'});
		let opUsername = createDOMElement('a', {
			'class': 'commenter-anchor', 'href': entryMeta.author.web_url, 'target': '_blank'
		}, entryMeta.author.username);
		opInfo.appendChild(opUsername);
		let opDate = createDOMElement('span', {'class': 'creation-date'}, ' on ');
		let opCreationDate = createDOMElement('span', {
			'class': 'date-time', 'data-toggle': 'tooltip', 'title': new Date(entryMeta.created_at).toTimeString()
		}, new Date(entryMeta.created_at).toDateString());
		opDate.appendChild(opCreationDate);
		if (entryMeta.created_at !== entryMeta.updated_at) {
			opDate.innerHTML += ' (Edited: ';
			let opUpdateDate = createDOMElement('span', {
				'class': 'date-time', 'data-toggle': 'tooltip', 'title': new Date(entryMeta.updated_at).toTimeString()
			}, new Date(entryMeta.updated_at).toDateString());
			opDate.appendChild(opUpdateDate);
			opDate.innerHTML += ')';
		}
		opInfo.appendChild(opDate);
		originalPost.appendChild(opInfo);
		contentDiv.appendChild(originalPost);
		contentDiv.appendChild(createDOMElement('hr'));
		appendElementWithStringAsset(contentDiv, '.md', entryMeta.description);
		contentDiv.appendChild(createDOMElement('hr'));
		let commentsTitle = createDOMElement('h5', null, `Comments • ${entryMeta.user_notes_count}`);
		contentDiv.appendChild(commentsTitle);
		if (!localStorage.gitlabAccessToken) {
			let notSignedInDiv = createDOMElement('div', {'id': 'not-signed-in'});
			let notSignedInP = createDOMElement('p');
			let signInButton = createDOMElement('a', {'class': 'gitlab-button', 'onclick': 'gitlabAuthenticate();'}, 'Sign in');
			notSignedInP.appendChild(signInButton);
			notSignedInP.innerHTML += ' using GitLab to post comments.';
			notSignedInDiv.appendChild(notSignedInP);
			contentDiv.appendChild(notSignedInDiv);
			renderCommentSection(entryMeta);
		} else {
			const commentForm = createDOMElement('div', {'class': 'comment-form'});
			let userMeta = null;
			request('GET', `${gitlabApi}${userUrl}`, undefined, {
				Authorization: `Bearer ${JSON.parse(localStorage.gitlabAccessToken).access_token}`
			}, undefined, undefined, true)
				.then(function(res) {
					userMeta = JSON.parse(res);
				})
				.finally(function() {
					renderCommentForm(commentForm, entryMeta, userMeta);
					contentDiv.appendChild(commentForm);
					renderCommentSection(entryMeta, userMeta);
				});
		}
	}

	function renderCommentForm(commentForm, entryMeta, userMeta) {
		if (userMeta) {
			let userAvatarAnchor = createDOMElement('a', {'class': 'user-avatar-anchor', 'href': userMeta.web_url, 'target': '_blank'});
			let userAvatar = createDOMElement('img', {'class': 'user-avatar',  'src': userMeta.avatar_url});
			userAvatarAnchor.appendChild(userAvatar);
			commentForm.appendChild(userAvatarAnchor);
		}
		let innerCommentForm = createDOMElement('div', {'class': 'comment-inner comment-form-inner'});
		innerCommentForm.appendChild(createDOMElement('textarea', {'name': 'body', 'id': 'comment-area', 'placeholder': 'Leave a comment', 'rows': '4'}));
		let commentButtons = createDOMElement('div');
		commentButtons.appendChild(createDOMElement('a', {
			'class': 'gitlab-button signout-button',
			'onclick': 'if (confirm(\'Are you sure you want to sign out?\')) {delete localStorage.gitlabAccessToken; window.location.reload(true);}'
		}, 'Sign out'));
		commentButtons.appendChild(createDOMElement('a', {
			'class': 'gitlab-button comment-button',
			'onclick': `postComment(${JSON.stringify(entryMeta)});`
		}, 'Comment'));
		innerCommentForm.appendChild(commentButtons);
		commentForm.appendChild(innerCommentForm);
	}

	function renderCommentSection(entryMeta, userMeta) {
		const issueCommentsUrl = `${gitlabApi}${issuesResource}/${entryMeta.iid}/notes`;
		const impersonationReq = {
			'method': 'get',
			'url': issueCommentsUrl,
			'params': {
				'sort': 'desc'
			}
		};
		request('POST', `${api}${impersonationResource}`, undefined, {'Content-Type': 'application/json'}, impersonationReq, undefined, true)
			.then(function(res) {
				let commentsMeta = JSON.parse(res);
				let commentSection = document.getElementById('comment-section') || createDOMElement('div', {'id': 'comment-section'});
				for (let i = 0; i < commentsMeta.length; i++) {
					if (!commentsMeta[i].system) {
						let commentDiv = createDOMElement('div', {'class': 'comment-outer'});
						let userAvatarAnchor = createDOMElement('a', {
							'class': 'user-avatar-anchor',
							'href': commentsMeta[i].author.web_url,
							'target': '_blank'
						});
						userAvatarAnchor.appendChild(createDOMElement('img', {'class': 'user-avatar', 'src': commentsMeta[i].author.avatar_url}));
						commentDiv.appendChild(userAvatarAnchor);
						let commentInner = createDOMElement('div', {'class': 'comment-inner'});
						let commentHeader = createDOMElement('div', {'class': 'comment-header'});
						commentHeader.appendChild(createDOMElement('a', {
							'class': 'commenter-anchor',
							'href': commentsMeta[i].author.web_url,
							'target': '_blank'
						}, commentsMeta[i].author.username));
						let commentDate = createDOMElement('span', {'class': 'creation-date'}, ' on ');
						commentDate.appendChild(createDOMElement('span', {
							'class': 'date-time',
							'data-toggle': 'tooltip',
							'title': new Date(commentsMeta[i].created_at).toTimeString()
						}, new Date(commentsMeta[i].created_at).toDateString()));
						if (commentsMeta[i].created_at !== commentsMeta[i].updated_at) {
							commentDate.innerHTML += ' (Edited: ';
							commentDate.appendChild(createDOMElement('span', {
								'class': 'date-time',
								'data-toggle': 'tooltip',
								'title': new Date(commentsMeta[i].updated_at).toTimeString()
							}, new Date(commentsMeta[i].updated_at).toDateString()));
							commentDate.innerHTML += ')';
						}
						commentHeader.appendChild(commentDate);
						let commentOptions = createDOMElement('div', {'class': 'comment-options'});
						if (userMeta && userMeta.username === commentsMeta[i].author.username) {
							commentOptions.appendChild(createDOMElement('a', {
								'class': 'comment-option',
								'data-toggle': 'tooltip',
								'title': 'delete comment',
								'onclick': `deleteComment("${issueCommentsUrl}/${commentsMeta[i].id}")`
							}, svg_x));
						}
						commentHeader.appendChild(commentOptions);
						commentInner.appendChild(commentHeader);
						commentInner.appendChild(createDOMElement('div', {'class': 'comment-body'}, md.render(commentsMeta[i].body)));
						commentDiv.appendChild(commentInner);
						commentSection.appendChild(commentDiv);
					}
				}
				contentDiv.appendChild(commentSection);
			})
			.catch(function(err) {
				alert(err + ': Failed to refresh comments section.');
			});
	}

	postComment = function(entryMeta) {
		const commentArea = document.getElementById('comment-area');
		if (commentArea && commentArea.value) {
			if (commentArea.value) {
				const issueCommentsUrl = `${gitlabApi}${issuesResource}/${entryMeta.iid}/notes`;
				request('POST', issueCommentsUrl, {body: commentArea.value}, {Authorization: `Bearer ${JSON.parse(localStorage.gitlabAccessToken).access_token}`})
					.then(function(response) {
						window.location.reload(true);
					});
			}
		}
	};

	deleteComment = function(commentUrl) {
		if (confirm('Are you sure you want to delete this comment?')) {
			request('DELETE', commentUrl, undefined, {Authorization: `Bearer ${JSON.parse(localStorage.gitlabAccessToken).access_token}`})
				.then(function(response) {
					window.location.reload(true);
				});
		}
	};

	if (contentDiv) {
		const blog = getURLParameter('id');
		const impersonationReq = {
			'method': 'get',
			'url': `${gitlabApi}${issuesResource}`,
			'params': {
				'labels': 'Live',
				'milestone': 'Blog',
				'state': 'opened'
			}
		};
		if (window.location.pathname.split('/').pop() === 'blog.html' && blog) {
			request('POST', `${api}${impersonationResource}`, undefined, {'Content-Type': 'application/json'}, impersonationReq, undefined, true)
				.then(function(response) {
					let entryMeta = JSON.parse(response).find(function(element) {
						return element.id.toString() === blog;
					});
					if (entryMeta) {
						renderBlogEntry(entryMeta);
					} else {
						window.location.href = '404.html';
					}
				})
				.catch(function(error) {
					window.location.href = '404.html';
				});
		} else {
			contentDiv.appendChild(createDOMElement('h4', null, 'Entries'));
			request('POST', `${api}${impersonationResource}`, undefined, {'Content-Type': 'application/json'}, impersonationReq)
				.then(function(response) {
					contentDiv.appendChild(createBlogList(JSON.parse(response)));
				});
		}
	}
}

window.addEventListener(eventInitializedName, renderBlogList, true);
