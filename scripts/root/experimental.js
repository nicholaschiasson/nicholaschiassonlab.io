// jshint esversion: 6

const experimentalProjectsPath = 'views/root/experimental';
const repositoryTreeResource = `${projectsResource}/${projectId}/repository/tree`;

function renderBlogList() {
	function createProjectListItem(projectName) {
		let item = createDOMElement('li');
		item.appendChild(createDOMElement('a', {
			'class': 'entry-title',
			'href': window.location + '?project=' + encodeURIComponent(projectName)
		}, projectName.replace(/\.(?=[^.]*$).*/, '')));
		return item;
	}

	function createProjectList(projects) {
		let list = createDOMElement('ul');
		for (let i = 0; i < projects.length; i++) {
			list.appendChild(createProjectListItem(projects[i].name));
		}
		return list;
	}

	function processExperimentalProjectsMeta(experimentalProjectsMeta) {
		let meta = JSON.parse(experimentalProjectsMeta);
		contentDiv.appendChild(createProjectList(meta));
	}

	function renderExperimentalProject(filename, response) {
		contentDiv.innerHTML = '';
		let returnButton = createDOMElement('a', {'class': 'back-button', 'href': 'experimental.html'});
		returnButton.appendChild(createDOMElement('h5', null, 'back to list'));
		contentDiv.appendChild(returnButton);
		appendElementWithStringAsset(contentDiv, filename, response);
		contentDiv.appendChild(returnButton.cloneNode(true));
	}

	let contentDiv = document.getElementById('content');

	if (contentDiv) {
		let project = getURLParameter('project');

		if (window.location.pathname.split('/').pop() === 'experimental.html' && project) {
			let filename = experimentalProjectsPath + '/' + project;
			request('GET', filename)
				.then(function(response) {
					renderExperimentalProject(filename, response);
				})
				.catch(function(error) {
					window.location.href = '404.html';
				});
		} else {
			const impersonationReq = {
				'method': 'get',
				'url': `${gitlabApi}${repositoryTreeResource}`,
				'params': {
					'path': experimentalProjectsPath
				}
			};
			request('POST', `${api}${impersonationResource}`, undefined, {'Content-Type': 'application/json'}, impersonationReq)
				.then(function(response) {
					processExperimentalProjectsMeta(response);
				});
		}
	}
}

window.addEventListener(eventInitializedName, renderBlogList, true);
