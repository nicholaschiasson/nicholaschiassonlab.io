// jshint esversion: 6

function renderProjectData() {
	function createProjectDataListItem(data) {
		let listItem = createDOMElement('li');
		let topDiv = createDOMElement('div', {'class': 'top'});
		if (data.title) {
			listItem.setAttribute('id', 'project-' + data.title);
			topDiv.appendChild(createDOMElement('h5', {'class': 'left'}, data.title));
		}
		let topRightDiv = createDOMElement('div', {'class': 'right'});
		if (data.home_url) {
			topRightDiv.appendChild(createDOMElement('a', {
				'class': 'left',
				'target': '_blank',
				'data-toggle': 'tooltip',
				'title': 'project home page',
				'href': data.home_url
			}, svg_home));
		}
		if (data.download_url) {
			topRightDiv.appendChild(createDOMElement('a', {
				'class': 'left',
				'target': '_blank',
				'data-toggle': 'tooltip',
				'title': 'project downloads',
				'href': data.download_url
			}, svg_download));
		}
		if (data.repo_url) {
			topRightDiv.appendChild(createDOMElement('a', {
				'class': 'left',
				'target': '_blank',
				'data-toggle': 'tooltip',
				'title': 'project repository',
				'href': data.repo_url
			}, svg_github));
		}
		if (data.doc_url) {
			topRightDiv.appendChild(createDOMElement('a', {
				'class': 'left',
				'target': '_blank',
				'data-toggle': 'tooltip',
				'title': 'project documentation',
				'href': data.doc_url
			} ,svg_book));
		}
		topRightDiv.appendChild(createDOMElement('a', {
			'class': 'left',
			'data-toggle': 'tooltip',
			'title': 'permanent link',
			'href': '#project-' + data.title
		}, svg_link));
		topDiv.appendChild(topRightDiv);
		listItem.appendChild(topDiv);
		let middleDiv = createDOMElement('div', {'class': 'middle'});
		if (data.description) {
			middleDiv.appendChild(createDOMElement('p', {'class': 'left'}, data.description));
		}
		if (data.thumbnail_url) {
			middleDiv.appendChild(createDOMElement('img', {'class': 'right', 'src': data.thumbnail_url}));
		}
		listItem.appendChild(middleDiv);
		let bottomDiv = createDOMElement('div', {'class': 'bottom'});
		if (data.license_badge) {
			bottomDiv.appendChild(createDOMElement('img', {'class': 'left', 'src': data.license_badge}));
		}
		listItem.appendChild(bottomDiv);
		return listItem;
	}

	function createProjectDataList(dataArray) {
		let list = createDOMElement('ul');
		for (let i = 0; i < dataArray.length; i++) {
			list.appendChild(createProjectDataListItem(dataArray[i]));
		}
		return list;
	}

	let contentDiv = document.getElementById('content');

	if (contentDiv) {
		contentDiv.appendChild(createDOMElement('h4', null, 'Personal'));
		if (ProjectData && ProjectData.personal) {
			contentDiv.appendChild(createProjectDataList(ProjectData.personal));
		}

		contentDiv.appendChild(createDOMElement('h4', null, 'Educational'));
		if (ProjectData && ProjectData.educational) {
			contentDiv.appendChild(createProjectDataList(ProjectData.educational));
		}

		contentDiv.appendChild(createDOMElement('h4', null, 'Jams'));
		if (ProjectData && ProjectData.jams) {
			contentDiv.appendChild(createProjectDataList(ProjectData.jams));
		}

		contentDiv.appendChild(createDOMElement('h4', null, 'Gists'));
		if (ProjectData && ProjectData.gists) {
			contentDiv.appendChild(createProjectDataList(ProjectData.gists));
		}
	}
}

window.addEventListener(eventInitializedName, renderProjectData, true);
