// jshint esversion: 6

const googleApi = 'https://www.googleapis.com';

const v2FilesResource = '/drive/v2/files';
const v3FilesResource = '/drive/v3/files';

const defaultPageSize = 32;
const mimeGoogleFolder = 'application/vnd.google-apps.folder';

function renderIndex(path, index) {
	function renderTitle() {
		if (contentDiv) {
			contentDiv.innerHTML = '';
			let basePath = createDOMElement('h6');
			let pathComponents = path.split('/');
			pathComponents.slice(0, -1).filter(component => component).forEach((component, i, list) => {
				let href = new URL(window.location);
				href.hash = '';
				href.search = `id=${list.slice(0, i + 1).map(c => { return encodeURIComponent(c); }).join('/')}`;
				basePath.appendChild(createDOMElement('a', {'href': href}, component));
				if (i < list.length - 1) {
					basePath.appendChild(createDOMElement('span', null, ' / '));
				}
			});
			contentDiv.appendChild(basePath);
			contentDiv.appendChild(createDOMElement('h4', null, pathComponents.slice(-1)[0]));
		}
	}

	function renderSearchBox() {
		if (contentDiv) {
			let searchForm = createDOMElement('form', {'class': 'search-form', 'method': 'get', 'formaction': window.location});
			searchForm.addEventListener('submit', dropURLHash);
			let urlParams = getURLQueryParameters();
			delete urlParams.search;
			for (let p in urlParams) {
				searchForm.appendChild(createDOMElement('input', {'type': 'hidden', 'name': p, 'value': urlParams[p]}));
			}
			let searchBox = createDOMElement('input', {'class': 'search-box', 'type': 'search', 'placeholder': 'Search this folder...', 'name': 'search'});
			searchForm.appendChild(searchBox);
			const search = getURLParameter('search');
			if (search) {
				let searchTitle = createDOMElement('h5', null, `Search: ${search}`);
				let searchClearButton = createDOMElement('a', {'class': 'clear-search-button'}, 'clear');
				searchClearButton.addEventListener('click', dropURLHash);
				searchClearButton.addEventListener('click', function(e) {
					searchBox.value = '';
					searchForm.submit();
				});
				searchTitle.appendChild(searchClearButton);
				searchForm.appendChild(searchTitle);
			}
			contentDiv.appendChild(searchForm);
		}
	}

	function updatePageCallback(newPage, search) {
		return function() {
			let statuses = document.getElementsByClassName('nav-status-indicator');
			for (let status of statuses) {
				status.innerHTML = 'Loading . . .';
			}
			location.hash = `page=${newPage}`;
			indexVideoLibrary(path, newPage, search)
				.then(res => {
					renderIndex(path, res);
				})
				.catch(err => {
					console.error(err);
					renderNoContent(new Error('Failed to load. The contents of the page may have changed. Refresh the page to load any changes.'));
				});
		};
	}

	function renderPageNavigation() {
		if (contentDiv && index.pages && index.pages.length > 1) {
			const search = getURLParameter('search');
			const page = Number(getURLParameter('page'));
			let navButtonsDiv = createDOMElement('div');
			let firstPageButton = createDOMElement('div', {
				'class': 'page-nav-button',
				'data-toggle': 'tooltip',
				'title': 'first page'
			}, svg_double_chevron_left);
			if (page !== 0) {
				firstPageButton.addEventListener('click', updatePageCallback(0, search));
			} else {
				firstPageButton.classList.add('disabled');
			}
			navButtonsDiv.appendChild(firstPageButton);
			let previousPageButton = createDOMElement('div', {
				'class': 'page-nav-button',
				'data-toggle': 'tooltip',
				'title': 'previous page'
			}, svg_chevron_left);
			if (page !== 0) {
				previousPageButton.addEventListener('click', updatePageCallback(page - 1, search));
			} else {
				previousPageButton.classList.add('disabled');
			}
			navButtonsDiv.appendChild(previousPageButton);
			let nextPageButton = createDOMElement('div', {
				'class': 'page-nav-button',
				'data-toggle': 'tooltip',
				'title': 'next page'
			}, svg_chevron_right);
			if (page !== index.pages.length - 1) {
				nextPageButton.addEventListener('click', updatePageCallback(page + 1, search));
			} else {
				nextPageButton.classList.add('disabled');
			}
			navButtonsDiv.appendChild(createDOMElement('span', {'class': 'nav-status-span'}, `Page ${page + 1} of ${index.pages.length}`));
			navButtonsDiv.appendChild(nextPageButton);
			let lastPageButton = createDOMElement('div', {
				'class': 'page-nav-button',
				'data-toggle': 'tooltip',
				'title': 'last page'
			}, svg_double_chevron_right);
			if (page !== index.pages.length - 1) {
				lastPageButton.addEventListener('click', updatePageCallback(index.pages.length - 1, search));
			} else {
				lastPageButton.classList.add('disabled');
			}
			navButtonsDiv.appendChild(lastPageButton);
			navButtonsDiv.appendChild(createDOMElement('span', {'class': 'nav-status-span nav-status-indicator'}));
			contentDiv.appendChild(navButtonsDiv);
		}
	}

	function renderList() {
		if (contentDiv) {
			let files = index.files;
			let list = createDOMElement('ul');
			if (files.length > 0) {
				files.forEach(file => {
					let item = createDOMElement('li');
					let icon = svg_file;
					let color = 'white';
					if (file.mimeType === mimeGoogleFolder) {
						icon = svg_file_directory;
						color = 'gold';
					} else if (file.mimeType.startsWith('video/')) {
						icon = svg_device_camera_video;
						color = 'deepskyblue';
					}
					item.appendChild(createDOMElement('span', {style: `fill: ${color}; padding-right: 0.5em`}, icon));
					let href = new URL(window.location);
					href.search = `id=${path.split('/').filter(p => p).map(p => encodeURIComponent(p)).join('/')}/${encodeURIComponent(file.name)}`;
					href.hash = '';
					item.appendChild(createDOMElement('a', {href}, file.name));
					list.appendChild(item);
				});
				contentDiv.appendChild(list);
			} else {
				contentDiv.appendChild(createDOMElement('p', null, 'No content to display.'));
			}
		}
	}

	function renderVideo() {
		if (contentDiv) {
			let videoBaseName = path.split('/').slice(-1)[0].replace(/\.mp4$/, '').replace(/'/g, '\\\'');

			let tabBar = createDOMElement('div', {'class': 'tab'});
			let html5Button = createDOMElement('button', {'class': 'tablink', 'title': 'HTML5 video player'}, 'HTML5');
			let iframeButton = createDOMElement('button', {'class': 'tablink', 'title': 'Embedded youtube video player (captions not supported)'}, 'IFrame');
			tabBar.appendChild(html5Button);
			tabBar.appendChild(iframeButton);
			contentDiv.appendChild(tabBar);
			let html5Video = createDOMElement('video', {'id': 'HTML5-content', 'width': '100%', 'controls': '', 'class': 'video-player tabcontent', 'crossorigin': 'anonymous'},
				'Your browser does not support the video tag.');
			contentDiv.appendChild(html5Video);
			html5Video.addEventListener('click', function() {html5Video.focus({preventScroll: true});});
			html5Video.addEventListener('keyup', function(e) {
				switch (e.key) {
					case 'f': {
						if (!document.fullscreenElement && !document.mozFullscreenElement && !document.webkitFullscreenElement) {
							if (html5Video.requestFullscreen) {
								html5Video.requestFullscreen();
							} else if (html5Video.mozRequestFullScreen) {
								html5Video.mozRequestFullScreen();
							} else if (html5Video.webkitRequestFullscreen) {
								html5Video.webkitRequestFullscreen();
							}
						} else {
							if (document.exitFullscreen) {
								document.exitFullscreen();
							} else if (document.mozExitFullscreen) {
								document.mozExitFullscreen();
							} else if (document.webkitExitFullscreen) {
								document.webkitExitFullscreen();
							}
						}
						break;
					}
					default:
						break;
				}
			});
			html5Video.addEventListener('error', function() {
				let currentSeekTime = html5Video.currentTime;
				html5Video.load();
				let videoReloaded = function(e) {
					html5Video.currentTime = currentSeekTime;
					html5Video.play();
					html5Video.removeEventListener('canplay', videoReloaded);
				};
				html5Video.addEventListener('canplay', videoReloaded);
			});
			let videoSource = createDOMElement('source', {
				'src': `${googleApi}${v3FilesResource}/${index.id}?access_token=${JSON.parse(localStorage.googleAccessToken).access_token}&alt=media`,
				'type': index.mimeType
			});
			window.addEventListener(eventRefreshTokenName, function() {
				videoSource.src = `${googleApi}${v3FilesResource}/${index.id}?access_token=${JSON.parse(localStorage.googleAccessToken).access_token}&alt=media`;
			});
			html5Video.appendChild(videoSource);
			window.vsrc = html5Video;

			// Fetch and add subtitles tracks
			request('GET', `${googleApi}${v3FilesResource}`, {
				orderBy: 'name',
				fields: 'files(id,name)',
				q: `'nicholasomerchiasson@gmail.com' in owners and name contains '${videoBaseName}' and (fileExtension = 'vtt' or name contains '.vtt.~') and '${index.parents[0].id}' in parents`
			}, {'Authorization': `Bearer ${JSON.parse(localStorage.googleAccessToken).access_token}`})
				.then(res => {
					for (let subs of JSON.parse(res).files) {
						const langCode = /\.[a-z]{2}\.vtt/.exec(subs.name)[0].split('.')[1];
						const subLabel = `${ianaLanguages[langCode].name}${/~[0-9]+~$/.test(subs.name) ? ` (${/~[0-9]+~$/.exec(subs.name)[0].split('~')[1]})` : ''}`;
						let subTitleTrack = createDOMElement('track', {
							'src': `${googleApi}${v3FilesResource}/${subs.id}?access_token=${JSON.parse(localStorage.googleAccessToken).access_token}&alt=media`,
							'kind': 'subtitles',
							'srclang': langCode,
							'label': subLabel
						});
						window.addEventListener(eventRefreshTokenName, function() {
							subTitleTrack.src = `${googleApi}${v3FilesResource}/${subs.id}?access_token=${JSON.parse(localStorage.googleAccessToken).access_token}&alt=media`;
						});
						html5Video.appendChild(subTitleTrack);
					}
				})
				.catch(err => {
					console.error(err);
				});
			html5Video.focus({preventScroll: true});
			contentDiv.appendChild(html5Video);
			let iframeVideo = createDOMElement('iframe', {
				'id': 'IFrame-content',
				'width': '100%',
				'height': '0',
				'class': 'video-player tabcontent',
				'src': index.embedLink,
				'frameborder': '0',
				'enablejsapi': '1',
				'allow': 'autoplay; encrypted-media',
				'allowfullscreen': 'true',
				'mozallowfullscreen': 'true',
				'webkitallowfullscreen': 'true'
			});
			let resizeVideo = function() { setTimeout(function() {
				html5Video.height = html5Video.clientWidth / 16.0 * 9.0;
				iframeVideo.height = iframeVideo.clientWidth / 16.0 * 9.0;
			}, 250); };
			html5Video.addEventListener('loadstart', resizeVideo);
			iframeVideo.addEventListener('load', resizeVideo);
			window.addEventListener('resize', resizeVideo);
			contentDiv.appendChild(iframeVideo);
			for (let button of tabBar.children) {
				button.addEventListener('click', e => {
					const targetContent = e.currentTarget.innerText;

					// Get all elements with class="tabcontent" and hide them
					let tabcontent = document.getElementsByClassName('tabcontent');
					for (let i = 0; i < tabcontent.length; i++) {
						tabcontent[i].style.display = 'none';
					}

					// Get all elements with class="tablink" and remove the class "active"
					let tablink = document.getElementsByClassName('tablink');
					for (let i = 0; i < tablink.length; i++) {
						tablink[i].className = tablink[i].className.replace(' active', '');
					}

					// Show the current tab, and add an "active" class to the button that opened the tab
					document.getElementById(`${targetContent}-content`).style.display = 'block';
					e.currentTarget.className += ' active';
					resizeVideo();
				});
			}
			html5Button.click();
		}
	}

	let contentDiv = document.getElementById('content');

	renderTitle();
	if (index) {
		if (index.files) {
			renderSearchBox();
			renderPageNavigation();
			renderList();
			renderPageNavigation();
		} else {
			renderVideo();
		}
	}
}

function renderNoContent(err) {
	let contentDiv = document.getElementById('content');
	if (contentDiv) {
		contentDiv.innerHTML = '';
		contentDiv.appendChild(createDOMElement('h3', null, 'No Content'));
		contentDiv.appendChild(createDOMElement('p', null, err));
	}
}

function retrievePageTokens(path, tokens, search, accumulator) {
	return new Promise((resolve, reject) => {
		let i = Number(accumulator) || 0;
		getPageId(path, i, search)
			.then(res => {
				resolve(retrievePageTokens(path, tokens.concat([res]), search, ++i));
			})
			.catch(err => {
				if (tokens.length > 0) {
					resolve(tokens);
				} else {
					reject(err);
				}
			});
	});
}

function getItemId(path, clearCache) {
	return cacheGet(`${window.location.pathname}:${path}`, () => {
		return new Promise((resolve, reject) => {
			if (!path) {
				reject('Invalid video path provided');
			}
			const parentPath = path.replace(/(.*)\/[^/]*$/, '$1');
			const itemName = path.replace(/.*\//, '').replace(/'/g, '\\\'');
			const originalQ = `'${projectOwner}' in owners and (mimeType contains 'video/' or mimeType = '${mimeGoogleFolder}') and name = '${itemName}'`;
			const params = {
				fields: 'files(id)',
				q: originalQ
			};
			tryOrInvalidate(invalidate => {
				let preStep = Promise.resolve();
				if (parentPath && parentPath !== path) {
					preStep = getItemId(parentPath, invalidate || clearCache)
						.then(res => {
							params.q = originalQ + ` and '${res}' in parents`;
						});
				}
				return preStep.then(() => request('GET', `${googleApi}${v3FilesResource}`, params, {'Authorization': `Bearer ${JSON.parse(localStorage.googleAccessToken).access_token}`}));
			}, (err, retryCb) => {
				if (!clearCache && parentPath && parentPath !== path) {
					return retryCb();
				}
				throw err;
			})
				.then(res => {
					let list = JSON.parse(res);
					if (list && list.files && list.files.length > 0) {
						resolve(list.files[0].id);
					} else {
						reject(`Item not found: ${path}`);
					}
				})
				.catch(reject);
		});
	}, clearCache);
}

function getPageId(path, page, search, clearCache) {
	return cacheGet(`${window.location.pathname}:${path}:${page}:search=${search || ''}`, () => {
		return new Promise((resolve, reject) => {
			if (page < 1) {
				resolve('');
			} else {
				tryOrInvalidate(invalidate => Promise.all([getItemId(path, invalidate), getPageId(path, page - 1, search, invalidate || clearCache)])
					.then(res => {
						let params = {
							fields: 'nextPageToken',
							orderBy: 'folder,name',
							pageSize: defaultPageSize,
							pageToken: res[1],
							q: `'nicholasomerchiasson@gmail.com' in owners and (mimeType contains 'video/' or mimeType = '${mimeGoogleFolder}') and '${res[0]}' in parents and name contains '${search || ''}'`
						};
						return request('GET', `${googleApi}${v3FilesResource}`, params, {'Authorization': `Bearer ${JSON.parse(localStorage.googleAccessToken).access_token}`});
					}), (err, retryCb) => {
					if (!clearCache) {
						return retryCb();
					}
					throw err;
				})
					.then(res => {
						let response = JSON.parse(res);
						if (response && response.nextPageToken) {
							resolve(response.nextPageToken);
						} else {
							reject(`Requested page out of range: ${page}`);
						}
					})
					.catch(reject);
			}
		});
	}, clearCache);
}

function indexVideoLibrary(path, page, search) {
	let i = (path || 'Videos').trim().replace(/\/+/g, '/').replace(/(^\/|\/$)/g, '');
	let p = Number(page) || 0;
	let s = (search || '').replace(/'/g, '\\\'');
	return new Promise((resolve, reject) => {
		tryOrInvalidate(invalidate => getItemId(i, invalidate)
			.then(res => request('GET', `${googleApi}${v2FilesResource}/${res}`, {
				fields: 'id,mimeType,parents,embedLink,alternateLink,webContentLink'
			}, {'Authorization': `Bearer ${JSON.parse(localStorage.googleAccessToken).access_token}`})
			)
		)
			.then(res => {
				const item = JSON.parse(res);
				if (item.mimeType === mimeGoogleFolder) {
					let index = {};
					tryOrInvalidate(invalidate => getPageId(i, p, s, invalidate)
						.then(res => {
							const params = {
								fields: 'files(id,mimeType,name,webViewLink)',
								orderBy: 'folder,name',
								pageSize: defaultPageSize,
								pageToken: res,
								q: `'nicholasomerchiasson@gmail.com' in owners and (mimeType contains 'video/' or mimeType = '${mimeGoogleFolder}') and '${item.id}' in parents and name contains '${s}'`
							};
							return request('GET', `${googleApi}${v3FilesResource}`, params, {'Authorization': `Bearer ${JSON.parse(localStorage.googleAccessToken).access_token}`});
						}))
						.then(res => {
							index = JSON.parse(res);
							return retrievePageTokens(i, [], s);
						})
						.then(res => {
							index.pages = res;
							resolve(index);
						})
						.catch(reject);
				} else {
					resolve(JSON.parse(res));
				}
			})
			.catch(reject);
	});
}

function initializeVideoLibrary() {
	const path = getURLParameter('id');
	const search = getURLParameter('search');
	const page = getURLParameter('page');
	if (!path) {
		window.location.search = `id=Videos&${new URLSearchParams(window.location.search).toString()}`;
	}
	setTimeout(function() {
		googleRefreshToken()
			.then(() => {
				return indexVideoLibrary(path, page, search);
			})
			.then(res => {
				if (res) {
					renderIndex(path, res);
				}
			})
			.catch(err => {
				console.error(err);
				renderNoContent(new Error('Could not retrieve video library either due to failed access token renewal or network error.'));
			});
	}, 500);
}

window.addEventListener(eventInitializedName, initializeVideoLibrary, true);
