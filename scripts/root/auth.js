// jshint esversion: 6

function getAccessTokensResource(provider) {
	return `/v1/oauth/${provider}/accessTokens`;
}

function setInnerHTML(element, innerHtml) {
	if (element) {
		element.innerHTML = innerHtml;
	}
}

function getAuthCode(provider) {
	switch (provider) {
		case 'gitlab':
		case 'google': {
			return getURLParameter('code');
		}
		default: {
			return null;
		}
	}
}

function getAccessToken(provider, authCode) {
	switch (provider) {
		case 'gitlab':
		case 'google': {
			return request('GET', `${api}${getAccessTokensResource(provider)}`, {application: projectName, code: authCode});
		}
		default: {
			return Promise.reject('Invalid authentication provider requested');
		}
	}
}

function returnToOrigin(originUrl) {
	if (originUrl && originUrl.includes(window.location.host)) {
		setTimeout(function() { window.location = originUrl; }, 1000);
	}
}

function checkAuthentication() {
	let header = document.getElementById('content-header');
	let state = getEncodedParameters(getURLParameter('state'));

	if (!state || !state.provider) {
		setInnerHTML(header, 'You have not been authenticated.');
	} else {
		let provider = state.provider;
		let originUrl = state.origin_url;
		let authCode = getAuthCode(provider);
		if (!authCode) {
			if (localStorage[`${provider}AccessToken`]) {
				setInnerHTML(header, '😊 You have been authenticated.');
				returnToOrigin(originUrl);
			} else {
				setInnerHTML(header, 'You have not been authenticated.');
			}
		} else {
			setInnerHTML(header, '🤔 Authenticating...');
			getAccessToken(provider, authCode)
				.then(function(res) {
					let accessToken = JSON.parse(res).access_token;
					if (!accessToken) throw new Error('Error: Failed to receive access token.');
					setInnerHTML(header, '😊 Authentication succeeded.');
					localStorage[`${provider}AccessToken`] = res;
					returnToOrigin(originUrl);
				})
				.catch(function(err) {
					setInnerHTML(header, '😢 Authentication failed.');
					alert(err + ': Failed to complete authentication.');
				});
		}
	}
}

window.addEventListener(eventInitializedName, checkAuthentication, true);
