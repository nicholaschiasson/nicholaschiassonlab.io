/* eslint-disable no-unused-vars */

ProjectData = {
	'educational': [
		{
			'title': 'snatch3d-vr',
			'description': 'Computer Science Undergraduate Honours Project - A virtual reality puzzle platformer game!',
			'thumbnail_url': 'https://raw.githubusercontent.com/nicholaschiasson/snatch3d-vr/master/Assets/Images/snatch3d-vr-icon-transparent-bg.png',
			'home_url': 'https://nicholaschiasson.github.io/snatch3d-vr',
			'download_url': 'https://github.com/nicholaschiasson/snatch3d-vr/releases/latest',
			'repo_url': 'https://github.com/nicholaschiasson/snatch3d-vr',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'affliction',
			'description': 'COMP4501 Final Project - An immune system real time strategy game made with Unity.',
			'thumbnail_url': '',
			'home_url': 'https://nicholaschiasson.github.io/affliction',
			'download_url': 'https://github.com/nicholaschiasson/affliction/releases/latest',
			'repo_url': 'https://github.com/nicholaschiasson/affliction',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'checkers',
			'description': 'COMP4501 Assignment 1 - A simple local multiplayer game of checkers made with Unity.',
			'thumbnail_url': '',
			'home_url': 'https://nicholaschiasson.github.io/checkers',
			'download_url': '',
			'repo_url': 'https://github.com/nicholaschiasson/checkers',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'N3DIL',
			'description': 'COMP3009 Final Project - Nick\'s 3D & Image Library. An OpenGL program rendering animated running manikins.',
			'thumbnail_url': '',
			'home_url': 'https://nicholaschiasson.github.io/N3DIL',
			'download_url': 'https://github.com/nicholaschiasson/N3DIL/releases/latest',
			'repo_url': 'https://github.com/nicholaschiasson/N3DIL',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'd00m3d',
			'description': 'COMP3501 Final Project - A space shooter game.',
			'thumbnail_url': '',
			'home_url': 'https://nicholaschiasson.github.io/d00m3d',
			'download_url': 'https://github.com/nicholaschiasson/d00m3d/releases/latest',
			'repo_url': 'https://github.com/nicholaschiasson/d00m3d',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'Snatch',
			'description': 'COMP2501 Final Project - A puzzle solving escape game!',
			'thumbnail_url': '',
			'home_url': 'https://nicholaschiasson.github.io/Snatch',
			'download_url': 'https://github.com/nicholaschiasson/Snatch/releases/latest',
			'repo_url': 'https://github.com/nicholaschiasson/Snatch',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'PlanetCiv: S.P.A.C.E',
			'description': 'COMP1501 Final Project - A planetary colonization game!',
			'thumbnail_url': '',
			'home_url': 'https://nicholaschiasson.github.io/PlanetCiv',
			'download_url': 'https://github.com/nicholaschiasson/PlanetCiv/releases/latest',
			'repo_url': 'https://github.com/nicholaschiasson/PlanetCiv',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'laser-pong',
			'description': 'COMP1405 Assignment 7 - An arcade survival style game of Pong!',
			'thumbnail_url': '',
			'home_url': 'https://nicholaschiasson.github.io/laser-pong',
			'download_url': 'https://github.com/nicholaschiasson/laser-pong/releases/latest',
			'repo_url': 'https://github.com/nicholaschiasson/laser-pong',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		}
	],
	'personal': [
		{
			'title': 'minesweeper',
			'description': 'Web implementation of basic minesweeper done in HTML, CSS, and Javascript for an interview developer challenge.',
			'thumbnail_url': 'https://images-na.ssl-images-amazon.com/images/I/61M9vdL-xjL.png',
			'home_url': 'https://nicholaschiasson.github.io/minesweeper/',
			'download_url': '',
			'repo_url': 'https://github.com/nicholaschiasson/minesweeper',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-The%20Unlicense-blue.svg'
		},
		{
			'title': 'car',
			'description': 'Compile and run! A command-line utility for quickly executing files containing code in a compiled language such as C, C++, Java, etc. Seamlessly executes files as if they were scripts, leaving no trace of binaries behind.',
			'thumbnail_url': 'https://www.misskatecuttables.com/uploads/shopping_cart/7778/large_car.png',
			'home_url': '',
			'download_url': 'https://github.com/nicholaschiasson/car/releases/latest',
			'repo_url': 'https://github.com/nicholaschiasson/car',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'W3DL',
			'description': 'Web 3D Library. An experimental graphics and math library built on WebGL.',
			'thumbnail_url': 'https://pidgi.net/wiki/images/thumb/9/92/Weedle_-_Pokemon_Dream_World.svg/164px-Weedle_-_Pokemon_Dream_World.svg.png',
			'home_url': '',
			'download_url': '',
			'repo_url': 'https://github.com/nicholaschiasson/W3DL',
			'doc_url': 'https://nicholaschiasson.github.io/W3DL',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'love-maker',
			'description': 'A simple distribution building script for your LÖVE games!',
			'thumbnail_url': 'https://avatars.githubusercontent.com/u/10260207?size=200',
			'home_url': 'https://nicholaschiasson.github.io/love-maker',
			'download_url': '',
			'repo_url': 'https://github.com/nicholaschiasson/love-maker',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-GPL--3.0-blue.svg'
		}
	],
	'jams': [
		{
			'title': 'Kill The Snowman',
			'description': 'Ludum Dare 31 entry - The title is a lie! Snowman is the best theme! This game was produced in less than 48 hours.',
			'thumbnail_url': 'https://github.com/nicholaschiasson/KillTheSnowman/raw/master/KillTheSnowman/Content/LudumDare31Icon.png',
			'home_url': 'https://nicholaschiasson.github.io/KillTheSnowman',
			'download_url': 'https://github.com/nicholaschiasson/KillTheSnowman/releases/latest',
			'repo_url': 'https://github.com/nicholaschiasson/KillTheSnowman',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		}
	],
	'gists': [
		{
			'title': 'cdmod',
			'description': 'A small set of bash aliases to effectively add backward and forward change directory functionality to the terminal, similar to how internet browser backward and forward navigation buttons work.',
			'thumbnail_url': '',
			'home_url': '',
			'download_url': '',
			'repo_url': 'https://gist.github.com/nicholaschiasson/18edfbbddc5ea8188a239dcb17159bde',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'ls_hidden',
			'description': 'Some bash functions and aliases to more easily list hidden dot files and directories.',
			'thumbnail_url': '',
			'home_url': '',
			'download_url': '',
			'repo_url': 'https://gist.github.com/nicholaschiasson/e2712d020a6317779555274f58b70f2f',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		},
		{
			'title': 'MemMon',
			'description': 'Memory monitors for C++ aiming to prevent double frees.',
			'thumbnail_url': '',
			'home_url': '',
			'download_url': '',
			'repo_url': 'https://gist.github.com/nicholaschiasson/85cd296f34c5a2728c5a61fb426242f8',
			'doc_url': '',
			'license_badge': 'https://img.shields.io/badge/license-MIT-blue.svg'
		}
	]
};
